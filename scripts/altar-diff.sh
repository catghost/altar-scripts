#!/usr/bin/env bash

[ $# -ge 2 ] || { echo "\
$0 - extract data.win archives using Altar.NET and diff the contents
usage: $0 <oldpath> <newpath>"; exit; }

oldpath="$1"
newpath="$2"

extract='altar-extract.sh'

tempdir="$(mktemp -d)"

# Extract old and new archives
echo "extracting old archive: $oldpath" 1>&2
"$extract" "$oldpath" "$tempdir/old" >/dev/null
echo "extracting new archive: $newpath" 1>&2
"$extract" "$newpath" "$tempdir/new" >/dev/null

# Diff extracted contents of archives
git diff "$tempdir/old" "$tempdir/new"

# Cleanup
rm -r "$tempdir"
