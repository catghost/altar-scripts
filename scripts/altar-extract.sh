#!/usr/bin/env bash

[ $# -ge 1 ] || { echo "\
$0 - extractor/beautifier script for data.win files (using Altar.NET)
usage: $0 <output>
       $0 <data.win> <output>"; exit; }

[ $# -eq 1 ] && outpath="$1"
[ $# -ge 2 ] && datapath="$1" && outpath="$2"

datapath="$1"
outpath="$2"

# Path to Altar.NET binary
altar='altar.exe'
# [ -x "$altar" ] || { echo "error: unable to find Altar.NET executable: $altar" 1>&2; exit 1; }

# Try to create output directory since Altar.NET won't
mkdir -p "$outpath" || { echo "error: unable to create output directory: $outpath" 1>&2; exit 1; }

# Extract data.win if infile is a zip file
extension="${datapath##*.}"
remove=0
if [ "$extension" == 'zip' ]; then
	tempdir="$(mktemp -d)"
	unzip "$datapath" 'data.win' -d "$tempdir" \
		|| { echo "error: unable to unzip data.win file from zip: $datapath" 1>&2; exit 1; }
	datapath="$tempdir/data.win"
	remove=1
fi

# Extract data.win file using Altar.NET
"$altar" export --any --noprecprog --out "$outpath" --file "$datapath"

# Beautify each JSON file using jq
find "$outpath" -type f -name '*.json' | while read -r path; do
	jq . "$path" > "${path}.tmp" && mv "${path}.tmp" "$path"
done

# If data.win file was extracted, remove parent directory
[ $remove -eq 1 ] && rm -r "$(dirname "$datapath")"
