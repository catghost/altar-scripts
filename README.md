altar-scripts
=============

This repository contains scripts for automating certain uses of [Altar.NET].

It currently includes:

* `altar-diff.sh` - Diff the extracted contents of two `data.win` files.
* `altar-extract.sh` - Extract a `data.win` file using Altar.NET and beautify
  the resulting JSON files using [jq].

For scripts that require the path of a `data.win` file as input, a `.zip` file which contains a `data.win` file may also be used.

[Altar.NET]:https://gitlab.com/catghost/Altar.NET
[jq]:https://stedolan.github.io/jq/
